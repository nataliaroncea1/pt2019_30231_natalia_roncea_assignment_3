package view;

/**
 * clasa care realizeaza interfata grafica
 */
import java.awt.Color;
import java.awt.Font;
import java.awt.Image;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.PrintStream;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.table.DefaultTableModel;

import controller.AfisareChitanta;

import controller.Serializare;
import model.Customer;
import model.OPDepartement;
import model.Order;
import model.Product;
import model.Warehouse;

public class  GUI extends JFrame {
	String str;
	int suma = 0;
	int nrSer = 0;

	/**
	 * constructorul clasei
	 */
	public GUI() {
		Warehouse w = new Warehouse();
		OPDepartement d = new OPDepartement();
		/**
		 * frame-ul principal
		 */
		JFrame f = new JFrame("");
		/**
		 * titlul
		 */
		JLabel t = new JLabel("Aplicatie de management al comenzilor");
		t.setBounds(60, 15, 500, 40);
		t.setForeground(new Color(255, 10, 50));
		t.setFont(new Font("Monotype Corsiva", Font.BOLD, 31));
		f.getContentPane().add(t);

		JButton b1 = new JButton("Enter");
		b1.setForeground(new Color(255, 10, 50));
		b1.setFont(new Font("Monotype Corsiva", Font.BOLD, 20));
		b1.setBounds(340, 140, 90, 50);
		f.getContentPane().add(b1);
		/**
		 * actiunea de pebutonul enter
		 */
		b1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				/**
				 * al doilea frame
				 */
				JFrame frame2 = new JFrame();

				JLabel l = new JLabel("Bine ati venit!--> alegeti una din optiuni:");
				l.setForeground(new Color(250, 10, 100));
				l.setFont(new Font("Monotype Corsiva", Font.BOLD, 30));
				l.setBounds(20, 30, 700, 48);
				frame2.getContentPane().add(l);

				JButton b = new JButton("Adauga/sterge produs");
				b.setBounds(180, 90, 200, 40);
				frame2.getContentPane().add(b);
				/**
				 * actiunea de pe butonul de daugare/stergere produs
				 */
				b.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						JFrame f3 = new JFrame("");
						JTable table = new JTable();
						JScrollPane pane = new JScrollPane(table);

						Object[] columns = { "ID", "Nume ", "Pret", "Categorie", "Cantitate" };
						DefaultTableModel model = new DefaultTableModel();
						model.setColumnIdentifiers(columns);
						table.setModel(model);

						table.setBackground(Color.white);
						table.setForeground(Color.red);
						Font font = new Font("Monotype Corsiva", 1, 18);
						table.setFont(font);
						table.setRowHeight(20);

						pane.setBounds(600, 100, 500, 300);

						JLabel titlu = new JLabel("Adaugare/stergerea unui produs nou in depozit:");
						titlu.setForeground(new Color(139, 0, 0));
						titlu.setFont(new Font("Monotype Corsiva", Font.PLAIN, 22));
						titlu.setBounds(150, 30, 268, 63);
						f3.getContentPane().add(titlu);

						JLabel idLabel = new JLabel("ID produs:");
						idLabel.setBounds(10, 110, 125, 20);
						f3.getContentPane().add(idLabel);

						JLabel numeLabel = new JLabel("Nume produs:");
						numeLabel.setBounds(10, 160, 125, 20);
						f3.getContentPane().add(numeLabel);

						JLabel pretLabel = new JLabel("Pret produs:");
						pretLabel.setBounds(10, 210, 125, 20);
						f3.getContentPane().add(pretLabel);

						JLabel catLabel = new JLabel("Categorie:");
						catLabel.setBounds(10, 260, 125, 20);
						f3.getContentPane().add(catLabel);

						JLabel cantLabel = new JLabel("Cantitate:");
						cantLabel.setBounds(10, 310, 125, 20);
						f3.getContentPane().add(cantLabel);

						JTextField idTextField = new JTextField();
						idTextField.setBounds(180, 110, 150, 30);
						idTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(idTextField);

						JTextField numeTextField = new JTextField();
						numeTextField.setBounds(180, 160, 150, 30);
						numeTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(numeTextField);

						JTextField pretTextField = new JTextField();
						pretTextField.setBounds(180, 210, 150, 30);
						pretTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(pretTextField);

						JTextField catTextField = new JTextField();
						catTextField.setBounds(180, 260, 150, 30);
						catTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(catTextField);

						JTextField cantTextField = new JTextField();
						cantTextField.setBounds(180, 310, 150, 30);
						cantTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(cantTextField);

						JButton fin = new JButton("Adauga produs");
						fin.setBounds(350, 110, 140, 30);
						f3.getContentPane().add(fin);

						JButton b2 = new JButton("Sterge produs");
						b2.setBounds(350, 190, 140, 30);
						f3.getContentPane().add(b2);

						JButton b3 = new JButton("Refresh");
						b3.setBounds(350, 150, 140, 30);
						f3.getContentPane().add(b3);
						/**
						 * butonul de refresh
						 */
						b3.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								idTextField.setText("");
								numeTextField.setText("");
								pretTextField.setText("");
								catTextField.setText("");
								cantTextField.setText("");

							}
						});

						Serializare ser = new Serializare();
						/**
						 * fin=butonul care adauga treptat cate un produs in
						 * depozit
						 */
						Object[] row = new Object[20];

						fin.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								nrSer++;

								String t = idTextField.getText();
								int id = Integer.parseInt(t);
								String t2 = numeTextField.getText();
								String t3 = pretTextField.getText();
								int pret = Integer.parseInt(t3);
								String t4 = catTextField.getText();
								String t5 = cantTextField.getText();
								int cant = Integer.parseInt(t5);

								Product p = new Product(id, t2, pret, t4, cant);

								w.add(p.getIdProdus(), p);
								/**
								 * afisam in tabel caracteristicile
								 */
								row[0] = p.getIdProdus();
								row[1] = p.getNumeProdus();
								row[2] = p.getPretProdus();
								row[3] = p.getCategorieProdus();
								row[4] = p.getCantitate();
								model.addRow(row);
								ser.serializ(model);

							}
						});
						/**
						 * deserializarea
						 */
						JButton bs = new JButton("Deserializare");
						bs.setBounds(350, 230, 140, 30);
						f3.getContentPane().add(bs);

						Object[] row1 = new Object[20];
						bs.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								try {

									FileInputStream fIn = new FileInputStream("Serial.ser");
									ObjectInputStream obj = new ObjectInputStream(fIn);

									
									DefaultTableModel model = (DefaultTableModel) obj.readObject();
								
					
                                 table.setModel(model);
									fIn.close();
									obj.close();

								} catch (IOException ioEx) {

									System.err.println(ioEx);

								} catch (ClassNotFoundException ex) {

									System.err.println(ex);

								}

							}
						});

						JButton bss = new JButton("Serializare");
						bss.setBounds(350, 300, 140, 30);
						f3.getContentPane().add(bss);

						JButton bmod = new JButton("Modificare");
						bmod.setBounds(350, 265, 140, 30);
						f3.getContentPane().add(bmod);
						/**
						 * buton pentru modificare datelor
						 */
						Object[] row2 = new Object[20];
						bmod.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								String t = idTextField.getText();
								int id = Integer.parseInt(t);
								String t2 = numeTextField.getText();
								String t3 = pretTextField.getText();
								int pret = Integer.parseInt(t3);
								String t4 = catTextField.getText();
								String t5 = cantTextField.getText();
								int cant = Integer.parseInt(t5);

								int i = table.getSelectedRow();
								if (i >= 0) {
									model.removeRow(i);
								}

								Product p = new Product(id, t2, pret, t4, cant);
								row2[0] = p.idProdus;
								row2[1] = p.numeProdus;
								row2[2] = p.pretProdus;
								row2[3] = p.categorieProdus;
								row2[4] = p.cantitate;

								model.addRow(row2);

							}
						});

						bss.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								// ser.serializ(0,"  ", 0," ", 0);

							}
						});
						/**
						 * stergerea unui produs din depozit si implicit din
						 * tabel
						 */
						b2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								int i = table.getSelectedRow();
								if (i >= 0) {
									model.removeRow(i);
								} else
									System.out.println("eroare la stergere");

								String t = idTextField.getText();
								int id = Integer.parseInt(t);
								String t2 = numeTextField.getText();
								String t3 = pretTextField.getText();
								int pret = Integer.parseInt(t3);
								String t4 = catTextField.getText();
								String t5 = cantTextField.getText();
								int cant = Integer.parseInt(t5);

								Product p = new Product(id, t2, pret, t4, cant);

								w.remove(p.getIdProdus(), p);

							}
						});
						/**
						 * pentru a reveni in frame-ul precedent
						 */
						JButton cancelButton = new JButton("Anuleaza operatie");
						cancelButton.setBounds(260, 380, 180, 30);
						f3.getContentPane().add(cancelButton);

						cancelButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								f3.setVisible(false);

							}
						});

						f3.getContentPane().add(pane);
						f3.getContentPane().setBackground(new Color(255, 255, 204));
						f3.setBounds(100, 100, 750, 600);
						f3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						f3.getContentPane().setLayout(null);
						f3.setSize(1200, 500);
						f3.setVisible(true);

					}
				});
				/**
				 * stergerea si adaugarea unei comenzi
				 */
				JButton bcom = new JButton("Adauga/sterge o comanda");
				bcom.setBounds(50, 200, 200, 40);
				frame2.getContentPane().add(bcom);

				// ---------------------------------------Adauga o
				// comanda---------------------------------
				bcom.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						/**
						 * frame-ul principal
						 */
						JFrame f3 = new JFrame("");

						JTable table = new JTable();
						JScrollPane pane = new JScrollPane(table);

						Object[] columns = { "ID comanda", "Nume expediere", "Adresa Expediere", "Nr tel contact",
								"Nume produs", "Pret produs", "Cantitate" };
						DefaultTableModel model = new DefaultTableModel();
						model.setColumnIdentifiers(columns);
						table.setModel(model);

						table.setBackground(Color.white);
						table.setForeground(Color.red);
						Font font = new Font("Monotype Corsiva", 1, 18);
						table.setFont(font);
						table.setRowHeight(20);

						pane.setBounds(520, 100, 630, 300);

						JLabel titlu = new JLabel("Adauga/sterge o comanda:");
						titlu.setForeground(new Color(139, 0, 0));
						titlu.setFont(new Font("Monotype Corsiva", Font.PLAIN, 22));
						titlu.setBounds(150, 30, 268, 63);
						f3.getContentPane().add(titlu);

						JLabel idLabel = new JLabel("ID comanda:");
						idLabel.setBounds(10, 110, 125, 20);
						f3.getContentPane().add(idLabel);

						JLabel numeLabel = new JLabel("Nume expediere");
						numeLabel.setBounds(10, 160, 125, 20);
						f3.getContentPane().add(numeLabel);

						JLabel pretLabel = new JLabel("Adresa expediere:");
						pretLabel.setBounds(10, 210, 125, 20);
						f3.getContentPane().add(pretLabel);

						JLabel catLabel = new JLabel("Numar telefon contact:");
						catLabel.setBounds(10, 260, 150, 20);
						f3.getContentPane().add(catLabel);

						JLabel cantLabel = new JLabel("Nume produs:");
						cantLabel.setBounds(10, 310, 125, 20);
						f3.getContentPane().add(cantLabel);

						JLabel pret = new JLabel("Pret produs:");
						pret.setBounds(10, 360, 125, 20);
						f3.getContentPane().add(pret);

						JLabel cant = new JLabel("Cantitate:");
						cant.setBounds(10, 410, 125, 20);
						f3.getContentPane().add(cant);

						JTextField idTextField = new JTextField();
						idTextField.setBounds(180, 110, 150, 30);
						idTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(idTextField);

						JTextField numeTextField = new JTextField();
						numeTextField.setBounds(180, 160, 150, 30);
						numeTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(numeTextField);

						JTextField adrTextField = new JTextField();
						adrTextField.setBounds(180, 210, 150, 30);
						adrTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(adrTextField);

						JTextField nrtelTextField = new JTextField();
						nrtelTextField.setBounds(180, 260, 150, 30);
						nrtelTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(nrtelTextField);

						JTextField numepTextField = new JTextField();
						numepTextField.setBounds(180, 310, 150, 30);
						numepTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(numepTextField);

						JTextField pretTextField = new JTextField();
						pretTextField.setBounds(180, 360, 150, 30);
						pretTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(pretTextField);

						JTextField cantTextField = new JTextField();
						cantTextField.setBounds(180, 410, 150, 30);
						cantTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(cantTextField);

						JButton fin = new JButton("Adauga comanda");
						fin.setBounds(350, 110, 140, 30);
						f3.getContentPane().add(fin);

						JButton b2 = new JButton("Sterge comanda");
						b2.setBounds(350, 190, 140, 30);
						f3.getContentPane().add(b2);

						JButton b3 = new JButton("Refresh");
						b3.setBounds(350, 150, 140, 30);
						f3.getContentPane().add(b3);

						JButton b4 = new JButton("Chitanta");
						b4.setBounds(350, 230, 140, 30);
						f3.getContentPane().add(b4);
						/**
						 * butonul de refresh
						 */
						b3.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								idTextField.setText("");
								numeTextField.setText("");
								adrTextField.setText("");
								nrtelTextField.setText("");
								numepTextField.setText("");
								pretTextField.setText("");
								cantTextField.setText("");

							}
						});

						Object[] row = new Object[7];
						/**
						 * actiunea indeplinita de butonul care adauga cate o
						 * comanda
						 */
						fin.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								String t = idTextField.getText();
								int id = Integer.parseInt(t);
								String t2 = numeTextField.getText();
								String t3 = adrTextField.getText();
								String t4 = nrtelTextField.getText();
								String t5 = numepTextField.getText();
								String t6 = cantTextField.getText();
								int ca = Integer.parseInt(t6);
								String t7 = pretTextField.getText();
								int pret = Integer.parseInt(t7);

								suma = suma + (pret * ca);
								Order o = new Order(id, t2, t3, t4, t5, ca, pret);
								Customer c = new Customer(t2, t3, t4);
								d.add(o.getIdComanda(), o);

								row[0] = o.getIdComanda();
								row[1] = o.getNumeExpediere();
								row[2] = o.getAdresaExpediere();
								row[3] = o.getNrTelefon();
								row[4] = o.getNumeProdus();
								row[5] = o.getPret();
								row[6] = o.getCantitate();
								model.addRow(row);

							}
						});
						/**
						 * actiunea indeplinita de butonul careafiseaza chitanta
						 * in fisierul text
						 */
						b4.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								String t = idTextField.getText();
								int id = Integer.parseInt(t);
								String t2 = numeTextField.getText();
								String t3 = adrTextField.getText();
								String t4 = nrtelTextField.getText();

								AfisareChitanta ch = new AfisareChitanta();
								ch.writech(id, t2, t3, t4, suma);

							}
						});
						/**
						 * stergerea unei comenzi
						 */
						b2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								int i = table.getSelectedRow();
								if (i >= 0) {
									model.removeRow(i);
								} else
									System.out.println("eroare la stergere");

								String t = idTextField.getText();
								int id = Integer.parseInt(t);
								String t2 = numeTextField.getText();
								String t3 = adrTextField.getText();
								String t4 = nrtelTextField.getText();
								String t5 = numepTextField.getText();
								String t6 = cantTextField.getText();
								int ca = Integer.parseInt(t6);
								String t7 = pretTextField.getText();
								int pret = Integer.parseInt(t7);

								Order o = new Order(id, t2, t3, t4, t5, ca, pret);
								Customer c = new Customer(t2, t3, t4);
								d.remove(o.getIdComanda(), o);

							}
						});
						/**
						 * modificare comanda
						 */
						JButton b2mod = new JButton("Modifica");
						b2mod.setBounds(350, 280, 140, 30);
						f3.getContentPane().add(b2mod);

						Object[] row3 = new Object[7];

						b2mod.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								String t = idTextField.getText();
								int id = Integer.parseInt(t);
								String t2 = numeTextField.getText();
								String t3 = adrTextField.getText();
								String t4 = nrtelTextField.getText();
								String t5 = numepTextField.getText();
								String t6 = cantTextField.getText();
								int ca = Integer.parseInt(t6);
								String t7 = pretTextField.getText();
								int pret = Integer.parseInt(t7);

								Order o = new Order(id, t2, t3, t4, t5, ca, pret);
								Customer c = new Customer(t2, t3, t4);

								int i = table.getSelectedRow();
								if (i >= 0) {
									model.removeRow(i);
								}
								row3[0] = o.getIdComanda();
								row3[1] = o.getNumeExpediere();
								row3[2] = o.getAdresaExpediere();
								row3[3] = o.getNrTelefon();
								row3[4] = o.getNumeProdus();
								row3[5] = o.getPret();
								row3[6] = o.getCantitate();
								model.addRow(row3);
							}
						});

						JButton cancelButton = new JButton("Anuleaza operatie");
						cancelButton.setBounds(260, 470, 180, 30);
						f3.getContentPane().add(cancelButton);

						cancelButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								f3.setVisible(false);

							}
						});

						f3.getContentPane().add(pane);
						f3.getContentPane().setBackground(new Color(255, 255, 204));
						f3.setBounds(100, 100, 741, 700);
						f3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						f3.getContentPane().setLayout(null);
						f3.setSize(1200, 600);
						f3.setVisible(true);
					}
				});

				// ------------------------------------------------------------------------------------
				/**
				 * adaugarea/stergerea/modificarea unui client
				 */
				JButton b3 = new JButton("Adauga/sterge client");
				b3.setBounds(300, 200, 200, 40);
				frame2.getContentPane().add(b3);

				b3.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						/**
						 * frame nou
						 */
						JFrame f3 = new JFrame("");
						JTable table = new JTable();
						JScrollPane pane = new JScrollPane(table);

						Object[] columns = { "Nume ", "Adresa", "Numar de telefon" };
						DefaultTableModel model = new DefaultTableModel();
						model.setColumnIdentifiers(columns);
						table.setModel(model);

						table.setBackground(Color.white);
						table.setForeground(Color.red);
						Font font = new Font("Monotype Corsiva", 1, 18);
						table.setFont(font);
						table.setRowHeight(20);

						pane.setBounds(600, 100, 500, 200);

						JLabel titlu = new JLabel("Adaugare/stergerea unui client :");
						titlu.setForeground(new Color(139, 0, 0));
						titlu.setFont(new Font("Monotype Corsiva", Font.PLAIN, 22));
						titlu.setBounds(150, 30, 268, 63);
						f3.getContentPane().add(titlu);

						JLabel idLabel = new JLabel("Nume client:");
						idLabel.setBounds(10, 110, 125, 20);
						f3.getContentPane().add(idLabel);

						JLabel numeLabel = new JLabel("Adresa client:");
						numeLabel.setBounds(10, 160, 125, 20);
						f3.getContentPane().add(numeLabel);

						JLabel pretLabel = new JLabel("Numar de telefon");
						pretLabel.setBounds(10, 210, 125, 20);
						f3.getContentPane().add(pretLabel);

						JTextField numeTextField = new JTextField();
						numeTextField.setBounds(180, 110, 150, 30);
						numeTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(numeTextField);

						JTextField adresaTextField = new JTextField();
						adresaTextField.setBounds(180, 160, 150, 30);
						adresaTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(adresaTextField);

						JTextField nrTextField = new JTextField();
						nrTextField.setBounds(180, 210, 150, 30);
						nrTextField.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(nrTextField);

						JButton fin = new JButton("Adauga client");
						fin.setBounds(350, 110, 140, 30);
						f3.getContentPane().add(fin);

						JButton b2 = new JButton("Sterge client");
						b2.setBounds(350, 190, 140, 30);
						f3.getContentPane().add(b2);

						JButton b3 = new JButton("Refresh");
						b3.setBounds(350, 150, 140, 30);
						f3.getContentPane().add(b3);
						/**
						 * 
						 * refresh :
						 */
						b3.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								numeTextField.setText("");
								adresaTextField.setText("");
								nrTextField.setText("");

							}
						});

						Object[] row = new Object[3];
						/**
						 * adaug clienti
						 */
						fin.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								String t = numeTextField.getText();
								String t2 = adresaTextField.getText();
								String t3 = nrTextField.getText();

								Customer c = new Customer(t, t2, t3);

								row[0] = c.getNume();
								row[1] = c.getAdresa();
								row[2] = c.getNrtel();

								model.addRow(row);

								numeTextField.setText("");
								adresaTextField.setText("");
								nrTextField.setText("");
							}
						});
						/**
						 * sterg clienti
						 */
						b2.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								int i = table.getSelectedRow();
								if (i >= 0) {
									model.removeRow(i);
								} else
									System.out.println("eroare la stergere");

							}
						});

						JButton b2mod = new JButton("Modificare");

						b2mod.setBounds(350, 240, 140, 30);
						f3.getContentPane().add(b2mod);
						/**
						 * modific date despre clienti
						 */
						Object[] row5 = new Object[3];
						b2mod.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {

								String t = numeTextField.getText();
								String t2 = adresaTextField.getText();
								String t3 = nrTextField.getText();

								Customer c = new Customer(t, t2, t3);

								row5[0] = c.getNume();
								row5[1] = c.getAdresa();
								row5[2] = c.getNrtel();

								model.addRow(row5);
								int i = table.getSelectedRow();
								if (i >= 0) {
									model.removeRow(i);
								}

							}
						});

						JButton cancelButton = new JButton("Anuleaza operatie");
						cancelButton.setBounds(150, 300, 180, 30);
						f3.getContentPane().add(cancelButton);

						cancelButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								f3.setVisible(false);

							}
						});

						f3.getContentPane().add(pane);
						f3.getContentPane().setBackground(new Color(255, 255, 204));
						f3.setBounds(100, 100, 750, 600);
						f3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						f3.getContentPane().setLayout(null);
						f3.setSize(1200, 400);
						f3.setVisible(true);

					}
				});
				/**
				 * cautarea unui produs dupa id si afisarea datelor
				 */
				// ----------------------------------------------------------------------------------------
				JButton b4 = new JButton("Cauta un produs dupa id");
				b4.setBounds(180, 320, 200, 40);
				frame2.getContentPane().add(b4);

				b4.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {

						JFrame f3 = new JFrame("");

						JLabel titlu = new JLabel("Cauta un produs dupa id:");
						titlu.setForeground(new Color(139, 0, 0));
						titlu.setFont(new Font("Monotype Corsiva", Font.PLAIN, 30));
						titlu.setBounds(80, 10, 400, 63);
						f3.getContentPane().add(titlu);

						JTextField add = new JTextField();
						add.setBounds(100, 100, 70, 30);
						add.setFont(new Font("Calibri", Font.BOLD, 16));
						f3.getContentPane().add(add);

						JButton adButton = new JButton("Cauta");
						adButton.setBounds(200, 100, 70, 30);
						f3.getContentPane().add(adButton);
						/**
						 * butonul care realizeaza cautarea
						 */
						adButton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								String s = add.getText();
								int key = Integer.parseInt(s);
								int tr = 0;
								for (Integer i : w.products.keySet()) {
									if (i == key) { // System.out.println("S-a gasit produsul cu id-ul "
													// + key);
										JOptionPane.showMessageDialog(
												null,
												"S-a gasit produsul cu id-ul " + key
														+ "si are caracteristicile :\n\n --id: "
														+ (w.products).get(key).idProdus + "\n --nume: "
														+ (w.products).get(key).numeProdus + "\n --pret: "
														+ (w.products).get(key).pretProdus + "\n --categorie: "
														+ (w.products).get(key).categorieProdus + "\n --cantitate: "
														+ (w.products).get(key).cantitate);
										tr = 1;
									}
								}
								if (tr == 0) {
									// System.out.println("Nu exista nici un produs cu id-ul "
									// + key);
									JOptionPane.showMessageDialog(null, "Nu s-a gasit nici un produs cu id-ul " + key);
								}
							}
						});

						JLabel idLabel = new JLabel("ID :");
						add.setFont(new Font("Calibri", Font.BOLD, 20));
						idLabel.setBounds(50, 100, 70, 20);
						f3.getContentPane().add(idLabel);

						f3.getContentPane().setBackground(new Color(255, 255, 204));
						f3.setBounds(100, 100, 500, 300);
						f3.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
						f3.getContentPane().setLayout(null);
						f3.setSize(500, 300);
						f3.setVisible(true);
					}
				});

				JLabel labelREZ = new JLabel("");
				labelREZ.setBackground(new Color(95, 158, 160));
				// labelREZ.setSize(600,400);
				//Image img = new ImageIcon(this.getClass().getResource("/java.jpg")).getImage();
				//labelREZ.setIcon(new ImageIcon(img));
				labelREZ.setBounds(0, 0, 600, 400);
				frame2.getContentPane().add(labelREZ);

				frame2.getContentPane().setBackground(new Color(255, 233, 200));
				frame2.setBounds(50, 50, 420, 400);
				frame2.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
				frame2.getContentPane().setLayout(null);
				frame2.setSize(618, 435);
				frame2.setVisible(true);
			}
		});

		JLabel labelREZ = new JLabel("");
		labelREZ.setBackground(new Color(95, 158, 160));
		// labelREZ.setSize(600,400);
		Image img = new ImageIcon(this.getClass().getResource("/new31.png")).getImage();
		labelREZ.setIcon(new ImageIcon(img));
		labelREZ.setBounds(0, 0, 600, 400);
		f.getContentPane().add(labelREZ);

		f.setBounds(100, 100, 610, 430);
		f.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		f.getContentPane().setLayout(null);
		f.setVisible(true);

	}

}
