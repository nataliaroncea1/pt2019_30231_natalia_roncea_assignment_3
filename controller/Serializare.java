package controller;

import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.IOException;
import java.io.Serializable;

import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

import model.Product;

/**
 * Clasa care faceserializarea unui produs in functie de caracteristicile
 * acestuia
 * 
 *
 *
 */
public class Serializare {
	/**
	 * metoda de serializare
	 * 
	 * @param idProdus
	 * @param numeProdus
	 * @param pretProdus
	 * @param categorieProdus
	 * @param cantitate
	 */
	public void serializ(DefaultTableModel t) {
		//Product p = new Product(idProdus, numeProdus, pretProdus, categorieProdus, cantitate);

		try {

			FileOutputStream fOut = new FileOutputStream("Serial.ser");
			ObjectOutputStream obj = new ObjectOutputStream(fOut);

			// Obiectul este serializat �i apoi este scris �n fi�ier.
			obj.writeObject(t);
			obj.flush();

			fOut.close();
			obj.close();

		} catch (IOException ioEx) {

			System.err.println(ioEx);

		}

	}

}