package controller;

import java.util.TreeMap;

import javax.swing.JOptionPane;

import model.OPDepartement;
import model.Product;

/**
 * clasa care implementeazametoda de testare unitara
 * 
 *
 *
 */
public class JUnit {
	public TreeMap<Integer, Product> products = new TreeMap<Integer, Product>();
	public int sizeMax = 200;
	public int nrProduse = 0;

	public String empty() {
		String str;
		if (products.isEmpty())
			str = "nu sunt produse!";
		else
			str = "arborele nu e gol!";

		return str;
	}
}
