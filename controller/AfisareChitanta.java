package controller;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import javax.swing.JOptionPane;

/**
 * clasa folosita pentru tiparirea unei chitante
 * 
 *
 *
 */
public class AfisareChitanta {
	/**
	 * constructor gol
	 */
	public AfisareChitanta() {
	}

	/**
	 * metoda care afiseaza chitanta intr-un fisier text
	 * 
	 * @param id
	 * @param nume
	 * @param adresa
	 * @param numar_telefon
	 * @param pret_total
	 */
	public void writech(int id, String nume, String adresa, String numar_telefon, int pret_total) {

		try {
			FileOutputStream fout = new FileOutputStream("chitanta.txt");
			PrintStream ps = new PrintStream(fout);
			ps.println("\n\n\n\t\t\tCHITANTA nr 10");
			ps.println("\n\n\n");
			ps.println("\n\t\tID COMANDA: " + id);
			ps.println("\n\t\tNUME EXPEDIERE: " + nume);
			ps.println("\n\t\tADRESA EXPEDIERE: " + adresa);
			ps.println("\n\t\tTELEFON DE CONTACT: " + numar_telefon);
			ps.println("\n\t\tPRET TOTAL COMANDA: " + pret_total);
			ps.println("\n\n");
			ps.println("\n\n\tDATA: 25.03.2016");
			JOptionPane.showMessageDialog(null, "Deschideti fisierul chitanta.txt !");

			ps.close();
			try {
				fout.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} catch (FileNotFoundException e) {
			System.out.println("eroare la afisare chitanta!");
			e.printStackTrace();
		}

	}
}