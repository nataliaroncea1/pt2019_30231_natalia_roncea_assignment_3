package model;

import java.io.Serializable;

/**
 * clasa ce implementeaza caracteristicile fiecarui produs
 * 
 *
 *
 */
public class Product implements Serializable {
	public int idProdus;
	public String numeProdus;
	public int pretProdus;
	public String categorieProdus;
	public int cantitate;

	/**
	 * clnstructorul clasei
	 * 
	 * @param idProdus
	 * @param numeProdus
	 * @param pretProdus
	 * @param categorieProdus
	 * @param cantitate
	 */
	public Product(int idProdus, String numeProdus, int pretProdus, String categorieProdus, int cantitate) {
		this.idProdus = idProdus;
		this.numeProdus = numeProdus;
		this.pretProdus = pretProdus;
		this.categorieProdus = categorieProdus;
		this.cantitate = cantitate;

	}

	/**
	 * metoda de afisare a produsului
	 */
	public void toString1() {
		String str = "Produs:\nid:" + this.idProdus + " ,nume produs:" + this.numeProdus + " pret produs:"
				+ this.pretProdus + " ,categorie:" + this.categorieProdus + " ,cantitate " + this.cantitate;
		System.out.println(str);
	}

	public int getIdProdus() {
		return idProdus;
	}

	public void setIdProdus(int idProdus) {
		this.idProdus = idProdus;
	}

	public String getNumeProdus() {
		return numeProdus;
	}

	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}

	public int getPretProdus() {
		return pretProdus;
	}

	public void setPretProdus(int pretProdus) {
		this.pretProdus = pretProdus;
	}

	public String getCategorieProdus() {
		return categorieProdus;
	}

	public void setCategorieProdus(String categorieProdus) {
		this.categorieProdus = categorieProdus;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}
}
