package model;

/**
 * clasa care implementeaza caracteristicile unui client
 * 
 *
 *
 */
public class Customer {
	String nume;
	String adresa;
	String nrtel;

	/**
	 * constructorul clasei
	 * 
	 * @param nume
	 * @param adresa
	 * @param nrtel
	 */
	public Customer(String nume, String adresa, String nrtel) {
		this.nume = nume;
		this.adresa = adresa;
		this.nrtel = nrtel;
	}

	/**
	 * gettere si settere
	 * 
	 * @return
	 */
	public String getNume() {
		return nume;
	}

	public void setNume(String nume) {
		this.nume = nume;
	}

	public String getAdresa() {
		return adresa;
	}

	public void setAdresa(String adresa) {
		this.adresa = adresa;
	}

	public String getNrtel() {
		return nrtel;
	}

	public void setNrtel(String nrtel) {
		this.nrtel = nrtel;
	}

}
