package model;

import java.io.Serializable;
import java.util.TreeMap;

import javax.swing.JOptionPane;

/**
 * clasa depozit ce stocheaza produse
 * 
 *
 *
 */

public class Warehouse implements Serializable{
	public TreeMap<Integer, Product> products = new TreeMap<Integer, Product>();
	public int sizeMax = 200;
	public int nrProduse = 0;

	/**
	 * constructor gol
	 */
	public Warehouse() {
	}

	/**
	 * metoda de adaugare
	 * 
	 * @param key
	 * @param P
	 */
	public void add(int key, Product P) {
		if (nrProduse < sizeMax) {
			products.put(key, P);
			nrProduse++;
			JOptionPane.showMessageDialog(null, "Produs adaugat(sunt:--" + nrProduse + "--produse)");
		} else
			JOptionPane.showMessageDialog(null, "Nu se mai pot stoca produse(max " + sizeMax + "produse)");
	}

	/**
	 * metoda de stergere
	 * 
	 * @param key
	 * @param P
	 */
	public void remove(int key, Product P) {
		if (nrProduse > 0) {

			products.remove(key);
			nrProduse--;

			JOptionPane.showMessageDialog(null, "Produs sters!(mai sunt sunt:--" + nrProduse + "--produse)");

		} else
			JOptionPane.showMessageDialog(null, "Nu mai sunt produse in stoc");
	}

	/**
	 * metoda care ne zice daca depozitul e gol
	 */

	public void empty() {
		if (products.isEmpty())
			System.out.println("nu sunt produse!");
		else
			System.out.println("arb nu e gol");

	}

	/**
	 * gettere si settere
	 * 
	 * @return
	 */
	public TreeMap<Integer, Product> getProducts() {
		return products;
	}

	public void setProducts(TreeMap<Integer, Product> products) {
		this.products = products;
	}

	public int getSizeMax() {
		return sizeMax;
	}

	public void setSizeMax(int sizeMax) {
		this.sizeMax = sizeMax;
	}

	public int getNrProduse() {
		return nrProduse;
	}

	public void setNrProduse(int nrProduse) {
		this.nrProduse = nrProduse;
	}

}
