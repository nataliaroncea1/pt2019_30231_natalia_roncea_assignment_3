package model;

import java.util.TreeMap;

import javax.swing.JOptionPane;

/**
 * clasa ce stocheza comenzile
 * 
 *
 *
 */
public class OPDepartement {
	public TreeMap<Integer, Order> orders = new TreeMap<Integer, Order>();
	public int orderMax = 200;
	public int nrOrders = 0;

	/**
	 * constructor gol
	 */
	public OPDepartement() {
	}

	/**
	 * al doilea constructor.acesta este cu parametrii
	 * 
	 * @param orders
	 * @param orderMax
	 * @param nrOrders
	 */
	public OPDepartement(TreeMap<Integer, Order> orders, int orderMax, int nrOrders) {
		this.orders = orders;
		this.orderMax = orderMax;
		this.nrOrders = nrOrders;
	}

	/**
	 * metoda de adaugare a unei comenzi
	 * 
	 * @param key
	 * @param o
	 */
	public void add(int key, Order o) {
		if (nrOrders < orderMax) {
			orders.put(key, o);
			nrOrders++;
			// System.out.println("key: "+key+" name: "+name);
			JOptionPane.showMessageDialog(null, "Comanda adaugata(sunt:--" + nrOrders + "--comenzi)");
		} else
			JOptionPane.showMessageDialog(null, "Nu se mai pot adauga comenzi(max " + orderMax + "comenzi)");
	}

	/**
	 * metoda de stergere a unei comenzi
	 * 
	 * @param key
	 * @param o
	 */
	public void remove(int key, Order O) {
		if (nrOrders > 0) {
			System.out.println("Before remove:");
			System.out.println(orders.toString());
			orders.remove(key);
			nrOrders--;

			JOptionPane.showMessageDialog(null, "Produs sters:(mai sunt sunt:--" + nrOrders + "--comenzi)");
			System.out.println("\nAfter remove:");
			System.out.println(orders.toString());
		} else
			JOptionPane.showMessageDialog(null, "Nu mai sunt comenzi de sters");
	}

	/**
	 * metoda care ne zice daca e gol
	 */
	public void empty() {
		if (orders.isEmpty())
			System.out.println("nu sunt comenzi!");
		else
			System.out.println("arb nu e gol");

	}

}
