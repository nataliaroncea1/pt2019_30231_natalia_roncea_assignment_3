package model;

/**
 * clasa ce contine toate caracteristicile unei comenzi
 */
import javax.swing.Spring;

public class Order {
	public int idComanda;
	public String numeExpediere;
	public String adresaExpediere;
	public String nrTelefon;
	public String numeProdus;
	public int cantitate;
	public int pret;

	/**
	 * constructorul clasei
	 * 
	 * @param idComanda
	 * @param numeExpediere
	 * @param adresaExpediere
	 * @param nrTelefon
	 * @param numeProdus
	 * @param cantitate
	 * @param pret
	 */
	public Order(int idComanda, String numeExpediere, String adresaExpediere, String nrTelefon, String numeProdus,
			int cantitate, int pret) {
		this.idComanda = idComanda;
		this.numeExpediere = numeExpediere;
		this.adresaExpediere = adresaExpediere;
		this.nrTelefon = nrTelefon;
		this.numeProdus = numeProdus;
		this.cantitate = cantitate;
		this.pret = pret;
	}

	/**
	 * gettere si settere
	 * 
	 * @return
	 */
	public int getPret() {
		return pret;
	}

	public void setPret(int pret) {
		this.pret = pret;
	}

	public int getIdComanda() {
		return idComanda;
	}

	public void setIdComanda(int idComanda) {
		this.idComanda = idComanda;
	}

	public String getNumeExpediere() {
		return numeExpediere;
	}

	public void setNumeExpediere(String numeExpediere) {
		this.numeExpediere = numeExpediere;
	}

	public String getAdresaExpediere() {
		return adresaExpediere;
	}

	public void setAdresaExpediere(String adresaExpediere) {
		this.adresaExpediere = adresaExpediere;
	}

	public String getNrTelefon() {
		return nrTelefon;
	}

	public void setNrTelefon(String nrTelefon) {
		this.nrTelefon = nrTelefon;
	}

	public String getNumeProdus() {
		return numeProdus;
	}

	public void setNumeProdus(String numeProdus) {
		this.numeProdus = numeProdus;
	}

	public int getCantitate() {
		return cantitate;
	}

	public void setCantitate(int cantitate) {
		this.cantitate = cantitate;
	}

}
